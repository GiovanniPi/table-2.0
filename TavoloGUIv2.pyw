# -*- coding: utf-8 -*-
"""
author: Giovanni Piccinato

Nuovo programma per la rotazione del tavolo, a differenza del precedente non
usa il threadm, ma il QTimer di QtCore. Il tasto connect permette di avviare e 
stoppare il timer, rendendo disponibile la risorsa manuale. Modifiche dello
stylesheet per avere grafica di bottoni, edit e groupbox custom
"""
from PyQt4 import QtGui, QtCore
from DriverTavolo2 import Tavolo, ErroreTavolo
import icon

style = """
QPushButton{
    
    color: #e5e5e5;
    background-color: QLinearGradient( x1: 0, y1: 0, x2: 1, y2: 1, stop: 0 #424242, stop: 1 #929292);
    border-width: 1px;
    border-color: #000000;
    border-style: solid;
    border-radius: 6;
    padding: 3px;
    font-size: 12px;
    padding-left: 5px;
    padding-right: 5px;
}
        
QPushButton:checked{
    background-color: QLinearGradient( x1: 0, y1: 0, x2: 1, y2: 1, stop: 0 #d2d2d2, stop: 1 #424242);
}

QGroupBox {
    border: 1px solid black;
    border-radius: 5px;
    margin-top: 1ex; /* leave space at the top for the title */
}

QLineEdit{
    background-color: QLinearGradient( x1: 0, y1: 0, x2: 1, y2: 1, stop: 0 #727272, stop: 1 #e2e2e2);
    padding: 1px;
    border-style: solid;
    border: 1px solid #1e1e1e;
    border-radius: 5px;
}

QGroupBox::title {
    subcontrol-origin: margin;
    subcontrol-position: top center; /* position at the top center */
    padding: -10px 5px;
}

QMainWindow{
    background-image: url(':/background.jpg');
}

QMessageBox{
    background-color: QLinearGradient(x1: 0, y1: 0, x2: 0, y2: 1, stop: 0 #bbbbbb, stop: 0.5 #ffffff, stop: 0.8 #f9f9f9, stop: 1 #e5e5e5);
}

QStatusBar{
    color: #000000;
}   
"""

class Table2(QtGui.QMainWindow):

    icon_red = r':/dot_red.png'
    icon_green = r':/dot_green.png'
    conn_flag = False

    def __init__(self, parent = None):
        super(Table2, self).__init__(parent)

#       -------------------------------------------------------GROUP CONNECTION
        btn_conn = self.creaBottone(self.connectTable, width = 75,
                                    nome = "Connect",
                                    tip = "Connect the table",
                                    toggled = True,
                                    icona = r':/sync.png')
        self.lab_pix_conn = QtGui.QLabel("")
        self.lab_pix_conn.setPixmap(QtGui.QPixmap(Table2.icon_red))

        grid_conn = QtGui.QGridLayout()
        grid_conn.addWidget(btn_conn, 1, 0, 1, 2)
        grid_conn.addWidget(self.lab_pix_conn, 1, 2, 1, 1)
        
#       -----------------------------------------------------------GROUP MOTION
        btn_move_sx = self.creaBottone(self.goUp, 
                                    tip = "Move the table clokwise",
                                    icona = r':/clockw.png')
        btn_move_dx = self.creaBottone(self.goDown,
                                   tip = "Move the table anti-clockwise",
                                   icona = r':/anitw.png')
        btn_stop = self.creaBottone(self.stop, tip = "Stop the table",
                                    icona = r':/stop.png')
        self.edit_pos = self.creaEdit(tip = "Insert a position for the table")
        btn_go = self.creaBottone(self.goTo, nome = "GO",
                                  tip = "Move the table to a given position",
                                  icona = r'icon/go.png')
        grid_move = QtGui.QGridLayout()
        grid_move.addWidget(btn_move_sx, 0, 0, 1, 1)
        grid_move.addWidget(btn_stop, 0, 1, 1, 1)
        grid_move.addWidget(btn_move_dx, 0, 2, 1, 1)
        grid_move.addWidget(QtGui.QLabel("GO TO POS:"), 1, 0, 1, 1)        
        grid_move.addWidget(self.edit_pos, 1, 1, 1, 1)
        grid_move.addWidget(btn_go, 1, 2, 1, 1)

#       ---------------------------------------------------------GROUP SETTINGS
        self.edit_max = self.creaEdit(tip = "Insert max rotation angle")
        self.edit_min = self.creaEdit(tip = "Insert min rotation angle")
        btn_set = self.creaBottone(self.setLimit, nome = "Set", 
                                   tip = "Send limits to the table",
                                   icona = r':/send.png')
        
        self.slider_sp = QtGui.QSlider(QtCore.Qt.Horizontal)
        self.slider_sp.setMaximum(8)
        self.slider_sp.setMinimum(1)
        self.slider_sp.setTickPosition(self.slider_sp.TicksBelow)
        self.slider_sp.setTickInterval(1)
        self.slider_sp.valueChanged[int].connect(self.setSpeed)
        self.lab_slider = QtGui.QLabel()
        
        grid = QtGui.QGridLayout()
        grid.addWidget(QtGui.QLabel("MAX"), 0, 0, 1, 1)
        grid.addWidget(self.edit_max, 0, 1, 1, 1)
        grid.addWidget(QtGui.QLabel("MIN"), 1, 0, 1, 1)
        grid.addWidget(self.edit_min, 1, 1, 1, 1)
        grid.addWidget(btn_set, 0, 2, 1, 2)
        grid.addWidget(QtGui.QLabel("SP"), 2, 0, 1, 1)
        grid.addWidget(self.slider_sp, 2, 1, 1, 1)
        grid.addWidget(self.lab_slider, 2, 2, 1, 1)       

#       ---------------------------------------------------------GROUP POSITION
        font = QtGui.QFont("Times",25,QtGui.QFont.Bold)
        self.lab_pos = QtGui.QLabel()
        self.lab_pos.setFont(font)
        self.lab_pos.setAlignment(QtCore.Qt.AlignCenter)
        self.lab_pos.setText("Nd")        
        
        box_pos = QtGui.QVBoxLayout()
        box_pos.addWidget(self.lab_pos)

#       ------------------------------------------------------------MAIN LAYOUT
        main_cmd = QtGui.QVBoxLayout()
        main_cmd.addWidget(self.creaGroup("Connection", grid_conn))
        main_cmd.addWidget(self.creaGroup("Movement", grid_move))
        main_cmd.addWidget(self.creaGroup("Settings", grid))
        main_cmd.addWidget(self.creaGroup("Position", box_pos))
     
#       -------------------------------------------------------------VARIABIILI
        Table2.conn_flag = False        
        self.timer = QtCore.QTimer(self)
        self.timer.timeout.connect(self.updatePos)
        
#       -----------------------------------------------------------SET FINESTRA
        central_wid = QtGui.QWidget()
        central_wid.setLayout(main_cmd)
        
        self.statusBar()
        self.statusBar().setSizeGripEnabled(False)
        self.setCentralWidget(central_wid)
        self.setWindowIcon(QtGui.QIcon(r':/table.png'))
        self.setWindowTitle("Table manager")
        self.setStyleSheet(style)
        self.setFixedSize(210, 350)

    def goUp(self):
        """Rotazione tavolo senso orario"""
        try:
            assert Table2.conn_flag
            self.stop()
            self.tavolo.avanti()
        except AssertionError:
            QtGui.QMessageBox.warning(self, "Error", 
                                     "You must connect the table before")
                                     
    def goDown(self):
        """Rotazione tavolo senso antiorario"""
        try:
            assert Table2.conn_flag
            self.stop()
            self.tavolo.indietro()
        except AssertionError:
            QtGui.QMessageBox.warning(self, "Error",
                                      "You must connect the table before")

    def goTo(self):
        try:
            assert Table2.conn_flag
            if self.edit_pos.text():
                value = int(self.edit_pos.text())
                self.tavolo.posizione = value
        except AssertionError:
            QtGui.QMessageBox.warning(self, "Error",
                                      "You must connect the table before")

    def stop(self):
        """Ferma movimento tavolo"""
        try:
            assert Table2.conn_flag
            self.tavolo.stop()
        except AssertionError:
            QtGui.QMessageBox.warning(self, "Error",
                                      "You must connect the table before")

    def setLimit(self):
        try:
            assert Table2.conn_flag
            self.tavolo.stop()
            minn = int(self.edit_min.text())
            maxx = int(self.edit_max.text())
            self.tavolo.limiti(minn, maxx)
        except AssertionError:
            QtGui.QMessageBox.warning(self, "Error", 
                                      "You must connect the table before")
        except ValueError:
            QtGui.QMessageBox.warning(self, "Error", 
                                      "Insert 2 integer numbers")
        except ErroreTavolo as e:
            QtGui.QMessageBox.warning(self, "Error", unicode(e))

    def setSpeed(self, value):
        """Setta velocità in base allo slider"""
        try:
            assert Table2.conn_flag
            if self.tavolo.busy:
                raise(Exception)
            self.tavolo.speed = value
            self.lab_slider.setText(str(self.tavolo.speed))
        except AssertionError:
            QtGui.QMessageBox.warning(self, "Error",
                                      "You must connect the table before")
        except Exception:
            QtGui.QMessageBox.warning(self, "Error",
                        "You can't cange the speed while the table is moving")

    def connectTable(self, flag):
        """Connette il tavolo, fa partire timer e abilita flag connessione"""
        try:
            if flag:
                self.tavolo = Tavolo()
                self.lab_pix_conn.setPixmap(QtGui.QPixmap(Table2.icon_green))
                Table2.conn_flag = True
                self.edit_max.setText(str(self.tavolo.maxA))
                self.edit_min.setText(str(self.tavolo.minA))
                pos = int(self.tavolo.posizione)
                self.lab_pos.setText(str(pos) + u'\u00B0')
                self.lab_slider.setText(self.tavolo.speed)
                self.slider_sp.setValue(int(self.lab_slider.text()))
                self.timer.start(500)
            else:
                self.lab_pix_conn.setPixmap(QtGui.QPixmap(Table2.icon_red))
                Table2.conn_flag = False
                self.timer.stop()
        except ErroreTavolo as e:
            QtGui.QMessageBox.warning(self, "Error", unicode(e))
            
    def updatePos(self):
        """Aggiorna posizione label e ruota immagina"""
        if self.tavolo.busy:
            pos = int(self.tavolo.posizione)
            self.lab_pos.setText(str(pos) + u'\u00B0')
            
    def creaBottone(self, action, nome = None, width = 50, tip = None, 
                    icona = None, toggled = None):
        """Crea un bottone"""
        if nome:
            bottone = QtGui.QPushButton(nome)
        else:
            bottone = QtGui.QPushButton()
        if toggled:
            bottone.setCheckable(True)
            bottone.clicked[bool].connect(action)
        else:
            bottone.clicked.connect(action)
        bottone.setFixedWidth(width)
        if tip:
            bottone.setStatusTip(tip)
        if icona:
            bottone.setIcon(QtGui.QIcon(icona))
        return bottone
        
    def creaEdit(self, length = 4, width = 50, tip = None):
        """crea una lineEdit"""
        edit = QtGui.QLineEdit()
        edit.setMaximumWidth(width)
        edit.setMaxLength(length)
        if tip:
            edit.setStatusTip(tip)
        return edit
        
    def creaGroup(self, nome, layout):
        group = QtGui.QGroupBox(nome)
        group.setLayout(layout)
        return group
        
if __name__ == '__main__':
    import sys
    app = QtGui.QApplication(sys.argv)
    form = Table2()
    form.show()
    sys.exit(app.exec_())