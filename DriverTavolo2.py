# -*- coding: utf-8 -*-
"""
Created on Thu Sep 18 16:51:54 2014

@author: Giovanni Piccinato

Nuova class per il Tavolo, migliorato ordine, utilizzo @property, possibilità
di inserire un indirizzo GPIB diverso.
I nomi sono più sensati
"""

import visa

class ErroreTavolo(Exception):
    
    def __init__(self, value):
        self.value = value

    def __str__(self):
        return repr(self.value)    

class Tavolo(object):
    
    def __init__(self, address = 7):
        try:
            self.tb = visa.instrument("GPIB::%d" % address)
            self._max = int(self.tb.ask("WL"))
            self._min = int(self.tb.ask("CL"))
            self._sp = self.tb.ask("SP")
        except visa.VisaIOError:
            raise ErroreTavolo("No instrument connected to address %d"
                                % address)
                                
    @property
    def idn(self):
        print self.tb.ask("*IDN?")

    @property
    def maxA(self):
        return self._max
        #return value.strip()
        
    @property
    def minA(self):
        return self._min
        #return value.strip()
        
    @property
    def speed(self):
        return self._sp
        
    @speed.setter
    def speed(self, value):
        self.tb.write("LD %d SP"%value)
        self._sp = self.tb.ask("SP")
        
    @property
    def busy(self):
        return int(self.tb.ask("BU"))
        
    def get_posizione(self):
        return self.tb.ask("CP")
        
    def set_posizione(self, value):
        if value > self._max or value < self._min:
            raise ErroreTavolo("Value out of bound")
        else:
            stringa = "LD %d DG NP" % (value)
            self.tb.write(stringa)
            self.tb.write("GO")
    
    posizione = property(get_posizione, set_posizione)

    def limiti(self, mi = 0, ma = 360):
        """
        Setta i limiti di movimento del tavolo, default è 0-360
        """                
        if mi < -200:
            raise ErroreTavolo("Minimum value is -200")
        if ma > 400:
            raise ErroreTavolo("Maximum value is 400")
        if mi > ma:
            raise ErroreTavolo("Your maximum must be greater than minimum")
            
        stringa = "LD %d DG CL" % mi
        stringa2 = "LD %d DG WL" % ma
        self.tb.write(stringa)
        self.tb.write(stringa2)
        self._max = ma
        self._min = mi
    
    def stop(self):
        self.tb.write("ST")
    
    def avanti(self):
        self.tb.write("CW")
        
    def indietro(self):
        self.tb.write("CC")